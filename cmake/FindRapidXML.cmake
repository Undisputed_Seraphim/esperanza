find_path(RapidXML_INCLUDE_DIR
	NAMES rapidxml.hpp
	PATH_SUFFIXES rapidxml rapidxml-${RapidXML_FIND_VERSION}
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(RapidXML DEFAULT_MSG RapidXML_INCLUDE_DIR)
mark_as_advanced(RapidXML_INCLUDE_DIR)

if (RapidXML_FOUND AND NOT TARGET RapidXML::RapidXML)
	add_library(RapidXML::RapidXML INTERFACE IMPORTED)
	set_target_properties(RapidXML::RapidXML PROPERTIES
		INTERFACE_INCLUDE_DIRECTORIES "${RapidXML_INCLUDE_DIR}"
	)
endif()
