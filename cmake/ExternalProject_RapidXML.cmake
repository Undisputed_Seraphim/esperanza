cmake_minimum_required(VERSION 3.10)
include(ExternalProject)

ExternalProject_Add(ext-RapidXML
	PREFIX "${CMAKE_BINARY_DIR}/external"
	URL "https://downloads.sourceforge.net/project/rapidxml/rapidxml/rapidxml%201.13/rapidxml-1.13.zip"
	UPDATE_COMMAND ""
	CONFIGURE_COMMAND ""
	BUILD_COMMAND ""
	INSTALL_COMMAND ""
)