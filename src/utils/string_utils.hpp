#pragma once
#include <algorithm>
#include <cctype>
#include <charconv>
#include <stdexcept>
#include <string_view>
#include <type_traits>

inline bool iequals(std::string_view l, std::string_view r) {
	return std::equal(l.begin(), l.end(), r.begin(), r.end(), [](char a, char b) -> bool {
		return ::tolower(a) == ::tolower(b);
	});
}

template <typename T>
T lexical_cast(const char* str, size_t len, T default = 0) noexcept {
	static_assert(std::is_arithmetic<T>::value);
	if (len == 0) return -1;
	T val = 0;
	auto res = std::from_chars(str, str + len, val);
	return (res.ec == std::errc()) ? val : default;
}

template <typename T>
T lexical_cast(std::string_view sv, T default = 0) noexcept {
	return lexical_cast<T>(sv.data(), sv.size(), default);
}

template <typename C>
void tokenize(std::string_view str, char delim, C&& callback) {
	std::string_view::size_type offset = 0;
	std::string_view::size_type len = str.size();
	do {
		auto next = str.find(delim, offset);
		if (next >= len)
			next = len;
		auto slice = std::string_view(str.data() + offset, next - offset);
		if (!callback(slice))
			break;
		offset = next + 1;
	} while (offset < len);
}

inline bool str_to_bool(const char* str, size_t len) {
	if (len == 0) return false;
	auto sv = std::string_view(str, len);
	if (sv == "1"
		|| sv == "TRUE" || sv == "True"
		|| sv == "YES" || sv == "Yes"
		|| sv == "ON" || sv == "On") {
		return true;
	}
	if (sv == "0"
		|| sv == "FALSE" || sv == "False"
		|| sv == "NO" || sv == "No"
		|| sv == "OFF" || sv == "Off") {
		return false;
	}
	throw std::logic_error("Not a boolean-like string: " + std::string(sv));
}