#pragma once
#include <rapidxml.hpp>
#include <rapidxml_iterators.hpp>
#include <rapidxml_utils.hpp>

/**
Convenience XML routines that wrap around RapidXML.
 **/

namespace rx = rapidxml;

template <typename Callback>
void node_foreach(rx::xml_node<>* node, Callback&& cb) {
	for (auto i = rx::node_iterator<char>(node); i != decltype(i)(); ++i) {
		if (!cb(*i)) break;
	}
}

template<typename Callback>
void attribute_foreach(rx::xml_node<>& node, Callback&& cb) {
	for (auto i = rx::attribute_iterator<char>(&node); i != decltype(i)(); ++i) {
		if (!cb(*i)) break;
	}
}