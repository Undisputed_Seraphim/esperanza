#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <string>
#include <Windows.h>

// defined in fmemopen.cpp
extern void LastErrorMsg(void(*msg_handler)(const char*));

namespace {

auto wstr_to_utf8(const std::wstring& input) -> std::string {
	int utfLen = WideCharToMultiByte(CP_UTF8, 0, input.data(), -1, NULL, 0, NULL, 0);
	auto utfStr = std::string(utfLen, '\0');
	if (WideCharToMultiByte(CP_UTF8, 0, input.data(), (int)input.size(), utfStr.data(), (int)utfStr.size(), NULL, NULL) == 0) {
		LastErrorMsg(perror);
		return "";
	}
	return utfStr;
}

auto codepage_to_utf8(int codepage, const char* str, size_t len) -> std::string {
	if (len == 0) return "";
	int wstrLen = MultiByteToWideChar(codepage, 0, str, (int)len + 1, NULL, 0);
	auto wstr = std::wstring(wstrLen, L'\0');
	if (MultiByteToWideChar(932, 0, str, (int)len + 1, wstr.data(), (int)wstr.size()) == 0) {
		LastErrorMsg(perror);
		return "";
	}
	return wstr_to_utf8(wstr);
}

} // anonymous namespace

auto shiftjis_to_utf8(const std::string& input) -> std::string {
	return codepage_to_utf8(932, input.c_str(), input.size());
}

#else // _WIN32
#include <errno.h>
#include <iconv.h>
#include <string>
#include <string.h>

struct Iconv final {
	Iconv(const char* tocode, const char* fromcode)
		: ic_(::iconv_open(tocode, fromcode))
		, tocode_(tocode)
		, fromcode_(fromcode) {
		valid();
	}
	Iconv(const Iconv&) = delete;
	Iconv(Iconv&& other) noexcept : Iconv(other.tocode_.data(), other.fromcode_.data()) {
		other.ic_ = NULL;
		other.tocode_ = "";
		other.fromcode_ = "";
	}

	~Iconv() noexcept { close(); }

	bool valid() noexcept {
		if (ic_ <= 0) {
			perror("iconv_open");
			return false;
		}
		return true;
	}

	bool reset() noexcept {
		close();
		ic_ = ::iconv_open(tocode_.data(), fromcode_.data());
	}

	operator iconv_t() noexcept {
		return ic_;
	}

private:
	libiconv_t ic_;
	std::string tocode_, fromcode_;

	void close() noexcept {
		if (::iconv_close(ic_) < 0) {
			::perror("iconv_close");
		}
	}
};

static Iconv ShiftJisToUTF8 = Iconv("UTF-8", "SHIFT-JIS");

auto shiftjis_to_utf8(const std::string& input) -> std::string {
	return to_utf8(ShiftJisToUTF8, input.c_str(), input.size());
}

auto shiftjis_to_utf8(const char* str, size_t len)->std::string {
	return shiftjis_to_utf8(ShiftJisToUTF8, str, len);
}

#endif // _WIN32

auto noop(const std::string& input) -> std::string {
	return input;
}