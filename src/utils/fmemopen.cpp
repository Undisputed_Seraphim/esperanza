#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <errno.h>
#include <fcntl.h>
#include <io.h>
#include <stdio.h>
#include <sys/stat.h>
#include <windows.h>

void LastErrorMsg(void(*msg_handler)(const char*)) {
	DWORD err = GetLastError();
	char* lpMsgBuf;
	FormatMessageA(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		err,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)& lpMsgBuf,
		0, NULL);
	msg_handler(lpMsgBuf);
	LocalFree(lpMsgBuf);
}

FILE* fmemopen(void* buf, size_t len, const char* mode) {
	char tp[MAX_PATH - 13];
	char fn[MAX_PATH + 1];
	if (!GetTempPathA(sizeof(tp), tp)) {
		LastErrorMsg(perror);
		return NULL;
	}
	if (!GetTempFileNameA(tp, "tmp", 0, fn)) {
		LastErrorMsg(perror);
		return NULL;
	}
	SECURITY_ATTRIBUTES secAttrib{
		sizeof(SECURITY_ATTRIBUTES),
		NULL,
		FALSE
	};
	HANDLE hnd = CreateFileA(fn, GENERIC_READ | GENERIC_WRITE
							 , FILE_SHARE_READ | FILE_SHARE_READ
							 , &secAttrib
							 , CREATE_ALWAYS
							 , FILE_ATTRIBUTE_TEMPORARY | FILE_FLAG_DELETE_ON_CLOSE
							 , NULL);
	if (hnd == INVALID_HANDLE_VALUE) {
		LastErrorMsg(perror);
		return NULL;
	}
	int fd = _open_osfhandle((intptr_t)hnd, _O_APPEND);
	if (fd == -1) {
		perror("_open_osfhandle");
		return NULL;
	}
	FILE* f = _fdopen(fd, "wb+");
	if (f == NULL) {
		perror("_fdopen");
		return NULL;
	}
	size_t w = fwrite(buf, sizeof(char), len, f);
	if (w != len) {
		perror("fwrite");
		return NULL;
	}
	rewind(f);
	return f;
}
#endif // _WIN32