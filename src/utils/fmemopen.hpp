#pragma once
#include <fstream>
#include <stdio.h>

#ifdef _WIN32
FILE* fmemopen(void* buf, size_t len, const char* type);
#endif // _WIN32

template <class CharT, class Traits = std::char_traits<CharT>>
struct basic_memfstream : public std::basic_fstream<CharT, Traits> {
	basic_memfstream(CharT* buffer, std::streamsize size)
		: std::basic_fstream<CharT, Traits>(::fmemopen(buffer, size, "wb+")) {}
	basic_memfstream(std::vector<CharT>& buffer)
		: std::basic_fstream<CharT, Traits>(::fmemopen(buffer.data(), buffer.size(), "wb+")) {}

	~basic_memfstream() noexcept { close(); }
};

template <class CharT, class Traits = std::char_traits<CharT>>
struct basic_imemfstream : public std::basic_ifstream<CharT, Traits> {
	basic_imemfstream(CharT* buffer, std::streamsize size)
		: std::basic_ifstream<CharT, Traits>(::fmemopen(buffer, size, "rb")) {}
	basic_imemfstream(std::vector<CharT>& buffer)
		: std::basic_ifstream<CharT, Traits>(::fmemopen(buffer.data(), buffer.size(), "rb")) {}

	~basic_imemfstream() noexcept { close(); }
};

template <class CharT, class Traits = std::char_traits<CharT>>
struct basic_omemfstream : public std::basic_ofstream<CharT, Traits> {
	basic_omemfstream(CharT* buffer, std::streamsize size)
		: std::basic_ofstream<CharT, Traits>(::fmemopen(buffer, size, "wb+")) {}
	basic_omemfstream(std::vector<CharT>& buffer)
		: std::basic_ofstream<CharT, Traits>(::fmemopen(buffer.data(), buffer.size(), "wb+")) {}

	~basic_omemfstream() noexcept { close(); }
};

using memfstream = basic_memfstream<char>;
using imemfstream = basic_imemfstream<char>;
using omemfstream = basic_omemfstream<char>;