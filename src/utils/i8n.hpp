#pragma once
#include <string>

auto shiftjis_to_utf8(const std::string&)->std::string;

auto noop(const std::string&)->std::string;