#pragma once
#include <cstdint>
#include <sstream>

template <typename T>
struct tvec2 final {
	T x, y;
};

template <typename T>
struct tvec3 final {
	T x, y, z;
};

template <typename T>
struct tvec4 final {
	T w, x, y, z;
};

using fvec2 = tvec2<float>;
using ivec2 = tvec2<int32_t>;

using fvec3 = tvec3<float>;
using ucvec3 = tvec3<uint8_t>;
using vec3 = tvec3<int32_t>;

using fvec4 = tvec4<float>;
using ivec4 = tvec3<int32_t>;

template <typename T = float>
auto str_to_vec2(const std::string& sv) -> tvec2<T> {
	auto val = tvec2<T>{};
	auto iss = std::istringstream(sv);
	iss >> val.x;
	iss >> val.y;
	return val;
}

template <typename T = float>
auto str_to_vec3(const std::string& sv) -> tvec3<T> {
	auto val = tvec3<T>{};
	auto iss = std::istringstream(sv);
	iss >> val.x;
	iss >> val.y;
	iss >> val.z;
	return val;
}

template <typename T = float>
auto str_to_vec4(const std::string& sv) -> tvec4<T> {
	auto val = tvec4<T>{};
	auto iss = std::istringstream(sv);
	iss >> val.w;
	iss >> val.x;
	iss >> val.y;
	iss >> val.z;
	return val;
}