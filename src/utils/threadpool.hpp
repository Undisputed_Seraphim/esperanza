#pragma once

#include <vector>
#include <queue>
#include <memory>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <future>
#include <functional>
#include <stdexcept>

class threadpool {
public:
	threadpool(size_t threads = std::thread::hardware_concurrency());

	~threadpool();

	threadpool(const threadpool&) = delete;
	threadpool& operator=(const threadpool&) = delete;

	template<typename Fn, class... Args>
	auto enqueue(Fn&& f, Args&& ... args)->std::future<typename std::result_of<Fn(Args...)>::type>;

private:
	std::vector<std::thread> workers;
	std::queue<std::function<void()>> tasks;

	std::mutex queue_mutex;
	std::condition_variable condition;
	bool stop;
};

// the constructor just launches some amount of workers
inline threadpool::threadpool(size_t threads) : stop(false) {
	for (size_t i = 0; i < threads; ++i) {
		workers.emplace_back([this]() {
			while (true) {
				std::function<void()> task;

				{
					std::unique_lock<std::mutex> lock(this->queue_mutex);

					this->condition.wait(lock, [this]() {
						return this->stop || !this->tasks.empty();
					});

					if (this->stop && this->tasks.empty()) {
						return;
					}

					task = std::move(this->tasks.front());

					this->tasks.pop();
				}

				task();
			}
		});
	}
}

// add new work item to the pool
template<typename Fn, class... Args>
auto threadpool::enqueue(Fn&& fn, Args&& ... args) -> std::future<typename std::result_of<Fn(Args...)>::type> {
	using return_type = typename std::invoke_result<Fn, Args...>::type;

	auto task = std::make_shared<std::packaged_task<return_type()>>(std::bind(std::forward<Fn>(fn), std::forward<Args>(args)...));

	std::future<return_type> res = task->get_future();
	{
		std::unique_lock<std::mutex> lock(queue_mutex);

		// don't allow enqueueing after stopping the pool
		if (stop) {
			throw std::runtime_error("Trying to enqueue a job on a stopped threadpool!");
		}

		tasks.emplace([task]() {
			(*task)();
		});
	}
	condition.notify_one();
	return res;
}

// the destructor joins all threads
inline threadpool::~threadpool() {
	{
		std::unique_lock<std::mutex> lock(queue_mutex);
		stop = true;
	}
	condition.notify_all();
	for (std::thread& worker : workers) {
		worker.join();
	}
}

// Declared in main.cpp
extern threadpool tp;