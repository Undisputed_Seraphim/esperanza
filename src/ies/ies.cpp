#include <filesystem>
#include <fstream>
#include <map>
#include <string>
#include <sstream>
#include <iostream>

#include "ies.hpp"

using namespace std::literals;
namespace fs = std::filesystem;

std::unordered_map<std::string, IES_Table> IES;

#pragma pack(push, 2)
enum Type : int16_t {
	Number = 0,
	String = 1,
	String2 = 2
};

struct Ies_Header final {
	char tableName[64];
	uint32_t version;
	uint32_t dataOffset;
	uint32_t resourceOffset;
	uint32_t filesize;
	uint16_t unknown_1;
	uint16_t rows;
	uint16_t cols;
	uint16_t int_cols;
	uint16_t str_cols;
	uint16_t unk3;
};

struct Ies_Column final {
	char name[64];
	char name2[64];
	Type type;
	uint16_t unused;
	uint16_t index;
};
#pragma pack(pop)

inline void ies_decrypt_string(char* str, const size_t len) noexcept {
	for (size_t i = 0; i < len; ++i)
		if (str[i] != 0) str[i] ^= 0x01;
}

auto read_xor_str(std::istream& is) -> std::string {
	uint16_t len = 0;
	is.read((char*)&len, sizeof(len));
	auto str = std::string(len, 0);
	is.read(str.data(), len);
	ies_decrypt_string(str.data(), len);
	return str;
}

IES_Table::IES_Table() : name({}), str_cols({}), int_cols({}) {}

IES_Table::IES_Table(std::istream& is) : IES_Table() {
	is >> *this;
}

size_t IES_Table::col_size() const noexcept {
	return int_cols.size() + str_cols.size();
}

void IES_Table::iterate_int_at_row(size_t row, std::function<bool(std::string_view, int32_t)> callback) const {
	for (const auto& [colname, vals] : int_cols) {
		if (!callback(colname, vals[row])) {
			break;
		}
	}
}

void IES_Table::iterate_str_at_row(size_t row, std::function<bool(std::string_view, std::string_view)> callback) const {
	for (const auto& [colname, vals] : str_cols) {
		if (!callback(colname, vals[row])) {
			break;
		}
	}
}

auto IES_Table::to_csv() const -> std::string {
	auto ret = std::string();
	auto rows = std::vector<std::string>{};
	for (const auto& [key, val] : int_cols) {
		ret.append(key);
		ret.append(",");
		rows.resize(std::max(rows.size(), val.size()));
		for (size_t i = 0; i < val.size(); ++i) {
			rows[i].append(std::to_string(val[i]));
			rows[i].append(",");
		}
	}
	for (const auto& [key, val] : str_cols) {
		ret.append(key);
		ret.append(",");
		rows.resize(std::max(rows.size(), val.size()));
		for (size_t i = 0; i < val.size(); ++i) {
			rows[i].append(val[i]);
			rows[i].append(",");
		}
	}
	ret.back() = '\n';
	for (auto& row : rows) {
		row.back() = '\n';
		ret.append(row);
	}
	return ret;
}

std::istream& operator>>(std::istream& is, IES_Table& table) {
	is.seekg(0, std::ios::end);
	const size_t filesize = is.tellg();
	is.seekg(0, std::ios::beg);
	auto hdr = Ies_Header();
	is.read((char*)&hdr, sizeof(Ies_Header));
	if (hdr.filesize != filesize) {
		throw std::runtime_error("File size in header did not match stream.");
	}
	table.name = std::string(hdr.tableName);
	table.str_cols = decltype(table.str_cols)(hdr.str_cols);
	table.int_cols = decltype(table.int_cols)(hdr.int_cols);
	is.seekg(filesize - ((size_t)hdr.dataOffset + hdr.resourceOffset), std::ios::beg);
	auto int_col_map = std::map<uint16_t, std::string>{};
	auto str_col_map = std::map<uint16_t, std::string>{};
	auto column = Ies_Column{};
	for (uint16_t i = 0; i < hdr.cols; ++i) {
		is.read((char*)&column, sizeof(Ies_Column));
		ies_decrypt_string(column.name, sizeof(column.name));
		//ies_decrypt_string(column.name2, sizeof(column.name2));
		if (column.type == Type::Number) {
			int_col_map.emplace(column.index, column.name);
		}
		else {
			str_col_map.emplace(column.index, column.name);
		}
	}
	for (const auto& [idx, colname] : int_col_map) {
		table.int_cols[colname] = {};
		table.int_cols[colname].reserve(hdr.rows);
	}
	for (const auto& [idx, colname] : str_col_map) {
		table.str_cols[colname] = {};
		table.str_cols[colname].reserve(hdr.rows);
	}
	is.seekg(filesize - hdr.resourceOffset, std::ios::beg);
	for (uint16_t i = 0; i < hdr.rows; ++i) {
		uint32_t row_idx = 0;
		is.read((char*)&row_idx, sizeof(row_idx));
		[[maybe_unused]] auto classname = read_xor_str(is); // Can be ignored
		for (uint16_t i = 0; i < hdr.int_cols; ++i) {
			double d = 0;
			is.read((char*)&d, sizeof(d));
			table.int_cols[int_col_map[i]].emplace_back(static_cast<int32_t>(d));
		}
		for (uint16_t i = 0; i < hdr.str_cols; ++i) {
			table.str_cols[str_col_map[i]].emplace_back(read_xor_str(is));
		}
		is.seekg(hdr.str_cols, std::ios::cur);
	}
	return is;
}

std::ostream& operator<<(std::ostream& os, const IES_Table& table) {
	os << table.to_csv();
	return os;
}

IES_Table_RowMajor::IES_Table_RowMajor() : name(), column_names({}), rows({}) {}

IES_Table_RowMajor::IES_Table_RowMajor(std::istream& is) : IES_Table_RowMajor() {
	is >> *this;
}

auto IES_Table_RowMajor::to_csv() const -> std::string {
	auto ss = std::ostringstream();
	for (const auto& col : column_names) {
		ss << col << ',';
	}
	ss.seekp(-1);
	ss << '\n';
	for (const auto& row : rows) {
		for (const auto& ints : row.ints) {
			ss << ints << ',';
		}
		for (const auto& strs : row.strs) {
			ss << strs << ',';
		}
		ss.seekp(-1);
		ss << '\n';
	}
	return ss.str();
}

auto IES_Table_RowMajor::cref_row_by_classname(const std::string& classname) -> const std::vector<Row>::iterator& {
	return std::find_if(rows.begin(), rows.end(), [&](const Row& row) -> bool {
		return row.classname == classname;
	});
}

void IES_Table_RowMajor::sort_rows_by_index() {
	std::sort(rows.begin(), rows.end(), [](const Row& l, const Row& r) -> bool {
		return l.index > r.index;
	});
}

std::istream& operator>>(std::istream& is, IES_Table_RowMajor& table) {
	is.seekg(0, std::ios::end);
	const size_t filesize = is.tellg();
	is.seekg(0, std::ios::beg);
	auto hdr = Ies_Header();
	is.read((char*)&hdr, sizeof(Ies_Header));
	if (hdr.filesize != filesize) {
		throw std::runtime_error("File size in header did not match stream.");
	}
	table.name = std::string(hdr.tableName);
	table.rows = decltype(table.rows)(hdr.rows);
	is.seekg(filesize - ((size_t)hdr.dataOffset + hdr.resourceOffset), std::ios::beg);
	auto int_col_map = std::map<uint16_t, std::string>{};
	auto str_col_map = std::map<uint16_t, std::string>{};
	auto column = Ies_Column{};
	for (uint16_t i = 0; i < hdr.cols; ++i) {
		is.read((char*)&column, sizeof(Ies_Column));
		ies_decrypt_string(column.name, sizeof(column.name));
		//ies_decrypt_string(column.name2, sizeof(column.name2));
		if (column.type == Type::Number) {
			int_col_map.emplace(column.index, column.name);
		}
		else {
			str_col_map.emplace(column.index, column.name);
		}
	}
	for (const auto& kv : int_col_map) {
		table.column_names.emplace_back(kv.second);
	}
	for (const auto& kv : str_col_map) {
		table.column_names.emplace_back(kv.second);
	}
	is.seekg(filesize - hdr.resourceOffset, std::ios::beg);
	for (uint16_t i = 0; i < hdr.rows; ++i) {
		auto row = IES_Table_RowMajor::Row{};
		row.ints.reserve(hdr.int_cols);
		row.strs.reserve(hdr.str_cols);
		is.read((char*)&row.index, sizeof(row.index));
		row.classname = read_xor_str(is);
		for (uint16_t i = 0; i < hdr.int_cols; ++i) {
			double d = 0;
			is.read((char*)&d, sizeof(d));
			row.ints.emplace_back(static_cast<int32_t>(d));
		}
		for (uint16_t i = 0; i < hdr.str_cols; ++i) {
			row.strs.emplace_back(read_xor_str(is));
		}
		is.seekg(hdr.str_cols, std::ios::cur);
	}
	return is;
}

std::ostream& operator<<(std::ostream& os, const IES_Table_RowMajor& table) {
	os << table.to_csv();
	return os;
}