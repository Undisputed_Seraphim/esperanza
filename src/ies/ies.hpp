#pragma once
#include <filesystem>
#include <functional>
#include <iosfwd>
#include <vector>
#include <unordered_map>

// Column major format
struct IES_Table final {
	std::string name;
	std::unordered_map<std::string, std::vector<std::string>> str_cols;
	std::unordered_map<std::string, std::vector<int32_t>> int_cols;

	IES_Table();
	IES_Table(std::istream& is);
	IES_Table(const IES_Table&) = delete;
	IES_Table(IES_Table&&) = default;

	size_t col_size() const noexcept;
	void iterate_int_at_row(size_t row, std::function<bool(std::string_view, int32_t)> callback) const;
	void iterate_str_at_row(size_t row, std::function<bool(std::string_view, std::string_view)> callback) const;

	auto to_csv() const->std::string;

	friend std::istream& operator>>(std::istream&, IES_Table&);
	friend std::ostream& operator<<(std::ostream&, const IES_Table&);
};

// Experimental row major format
struct IES_Table_RowMajor final {
	struct Row final {
		int32_t index;
		std::string classname;
		std::vector<std::string> strs;
		std::vector<int32_t> ints;
	};
	std::string name;
	std::vector<std::string> column_names;
	std::vector<Row> rows;

	IES_Table_RowMajor();
	IES_Table_RowMajor(std::istream&);
	IES_Table_RowMajor(const IES_Table_RowMajor&) = delete;
	IES_Table_RowMajor(IES_Table_RowMajor&&) = default;

	auto to_csv() const->std::string;
	auto cref_row_by_classname(const std::string&) -> const std::vector<Row>::iterator&;
	void sort_rows_by_index();

	friend std::istream& operator>>(std::istream&, IES_Table_RowMajor&);
	friend std::ostream& operator<<(std::ostream&, const IES_Table_RowMajor&);
};