#include <array>
#include <iostream>
#include <vector>
#include <zlib.h>
#include "ipf.hpp"

using key_t = std::array<uint32_t, 3>;

constexpr auto PASSWORD = std::array<unsigned char, 48>{
	0x25, 0x66, 0x20, 0x25,
		0x66, 0x20, 0x25, 0x73,
		0x20, 0x25, 0x73, 0x20,
		0xff, 0xff, 0xff, 0xff,
		0x3f, 0x5b, 0x20, 0xff,
		0xff, 0xff, 0xff, 0x3f,
		0x25, 0x66, 0x20, 0x25,
		0x66, 0x20, 0x25, 0x73,
		0x20, 0x68, 0x20, 0x25,
		0x73, 0x20, 0x2e, 0x3f,
		0x2e, 0x20, 0x20, 0x20,
		0x58, 0xff, 0x24, 0x24
};

#pragma pack(push, 2)
struct Ipf_ZipCentralDirectoryHeader {
	int32_t signature;
	int16_t version_madeby;
	int16_t version_needed;
	int16_t flags;
	int16_t compress_method;
	int16_t last_modified_time;
	int16_t last_modified_date;
	int32_t crc32;
	int32_t compressed_size;
	int32_t decompressed_size;
	int16_t filename_length;
	int16_t extra_field_length;
	int16_t comment_length;
	int16_t disk_number;
	int16_t internal_attrib;
	int32_t external_attrib;
	int32_t local_offset_relative;
};

struct Ipf_ZipEndCentralRecord {
	int32_t signature;
	int16_t disk_number;
	int16_t central_start_disk;
	int16_t central_record_count_on_disk;
	int16_t central_record_total;
	int32_t central_record_size;
	int32_t central_record_offset;
	int16_t comment_length;
};

struct Ipf_ZipEntry {
	int32_t signature;
	int16_t version_needed;
	int16_t flags;
	int16_t compress_method;
	int16_t last_modified_time;
	int16_t last_modified_date;
	int32_t crc32;
	int32_t compressed_size;
	int32_t decompressed_size;
	int16_t filename_length;
	int16_t extra_field_length;
};
#pragma pack(pop)

inline uint32_t compute_crc32(uint32_t crc, unsigned char b) noexcept {
	return get_crc_table()[(crc ^ b) & 0xff] ^ (crc >> 8);
}

void key_update(key_t& keys, unsigned char b) noexcept {
	keys[0] = compute_crc32(keys[0], b);
	keys[1] = (keys[1] + (keys[0] & 0xff)) * 0x8088405 + 1;
	keys[2] = compute_crc32(keys[2], char(keys[1] >> 24));
}

auto key_init() noexcept -> key_t {
	key_t keys = { 0x12345678, 0x23456789, 0x34567890 };
	for (size_t i = 0; i < PASSWORD.size(); ++i)
		key_update(keys, PASSWORD[i]);
	return keys;
}

void decrypt(unsigned char* str, const size_t len, key_t& keys = key_init()) noexcept {
	for (size_t i = 0; i < len; ++i) {
		const uint16_t t = (keys[2] & 0xffff) | 2;
		str[i] ^= ((t ^ 1) * t >> 8);
		key_update(keys, str[i]);
	}
}

bool zlib_inflate(unsigned char* in, size_t in_len, unsigned char* out, size_t out_len) noexcept {
	z_stream stream;
	stream.next_in = in;
	stream.avail_in = (uInt)in_len;
	stream.next_out = out;
	stream.avail_out = (uInt)out_len;
	stream.zalloc = NULL;
	stream.zfree = NULL;
	if (inflateInit2(&stream, -MAX_WBITS) != Z_OK) {
		std::cerr << "Failed to init inflate2." << std::endl;
		inflateEnd(&stream);
		return false;
	}
	if (int ret = inflate(&stream, Z_FINISH); ret != Z_STREAM_END) {
		std::cerr << "Failed to inflate. Error was " << ret << std::endl;
		inflateEnd(&stream);
		return false;
	}
	if (inflateEnd(&stream) != Z_OK) {
		std::cerr << "Failed to end stream." << std::endl;
		return false;
	}
	return true;
}

bool zlib_inflate(std::vector<unsigned char>& in, std::vector<unsigned char>& out) {
	return zlib_inflate(in.data(), in.size(), out.data(), out.size());
}

auto get_all_entries(const std::filesystem::path& file) -> std::unordered_map<std::string, Entry> {
	constexpr int32_t ZIP_END_SIGNATURE = 0x06054b50;
	constexpr int32_t ZIP_RECORD_SIGNATURE = 0x02014b50;

	auto ifs = std::ifstream(file, std::ios::in | std::ios::binary | std::ios::ate);
	const size_t size = ifs.tellg();
	auto end = Ipf_ZipEndCentralRecord();
	ifs.seekg(size - sizeof(Ipf_ZipEndCentralRecord) - 1);
	ifs.read((char*)& end, sizeof(Ipf_ZipEndCentralRecord));
	if (end.signature != ZIP_END_SIGNATURE) {
		std::cerr << "Zip signature did not match!" << std::endl;
		return {};
	}
	ifs.seekg(end.central_record_offset);
	auto entries = std::unordered_map<std::string, Entry>(end.central_record_total);
	auto r = Ipf_ZipCentralDirectoryHeader();
	for (int i = 0; i < end.central_record_total; ++i) {
		ifs.read((char*)& r, sizeof(r));
		if (r.signature != ZIP_RECORD_SIGNATURE) {
			std::cerr << "Zip record signature did not match!" << std::endl;
			continue;
		}
		auto filename = std::string(r.filename_length, 0);
		ifs.read(filename.data(), r.filename_length);
		decrypt((unsigned char*)filename.data(), filename.size());
		if (filename.back() != '/') {
			entries.emplace(std::move(filename), Entry{ r.local_offset_relative, r.compressed_size, r.decompressed_size });
		}
		ifs.seekg(r.extra_field_length, std::ios_base::cur);
		ifs.seekg(r.comment_length, std::ios_base::cur);
	}
	return entries;
}

auto extract_file(std::ifstream& ifs, const Entry& entry) -> std::vector<unsigned char> {
	constexpr int32_t ZIP_LOCAL_SIGNATURE = 0x04034b50;
	constexpr int16_t ZIP_IS_ENCRYPTED_FLAG = 0x0001;
	auto c_buf = std::vector<unsigned char>(entry.compressed_size, 0);
	auto d_buf = std::vector<unsigned char>(entry.decompressed_size, 0);
	auto e = Ipf_ZipEntry();
	ifs.seekg(entry.offset);
	ifs.read((char*)& e, sizeof(e));
	if (e.signature != ZIP_LOCAL_SIGNATURE) {
		std::cerr << "Zip entry signature did not match!" << std::endl;
		return {};
	}
	ifs.seekg(e.filename_length, std::ios_base::cur);
	ifs.seekg(e.extra_field_length, std::ios_base::cur);
	if ((e.flags & ZIP_IS_ENCRYPTED_FLAG) != 0) {
		auto keys = key_init();
		unsigned char encrypt_header[12];
		ifs.read((char*)encrypt_header, sizeof(encrypt_header));
		decrypt(encrypt_header, sizeof(encrypt_header), keys);
		ifs.read((char*)c_buf.data(), entry.compressed_size);
		decrypt(c_buf.data(), c_buf.size(), keys);
	}
	else {
		ifs.read((char*)c_buf.data(), entry.compressed_size);
	}
	return zlib_inflate(c_buf, d_buf) ? d_buf : std::vector<unsigned char>();
}

auto extract_file(const std::filesystem::path& file, const Entry& entry) -> std::vector<unsigned char> {
	auto ifs = std::ifstream(file, std::ios::in | std::ios::binary);
	return extract_file(ifs, entry);
}