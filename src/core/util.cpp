#include <glm/glm.hpp>
#include <string_utils.hpp>
#include <xml.hpp>

auto text_to_vec3(std::string_view text) noexcept -> glm::fvec3 {
	auto vec = glm::fvec3();
	size_t j = 0;
	tokenize(text, ' ', [&](std::string_view sv) -> bool {
		switch (j++) {
		case 0: vec.x = lexical_cast<float>(sv, 0); break;
		case 1: vec.y = lexical_cast<float>(sv, 0); break;
		case 2: vec.z = lexical_cast<float>(sv, 0); break;
		default: return false;
		}
		return true;
	});
	return vec;
}

auto text_to_vec4(std::string_view text) noexcept -> glm::fvec4 {
	auto vec = glm::fvec4();
	size_t j = 0;
	tokenize(text, ' ', [&](std::string_view sv) -> bool {
		switch (j++) {
		case 0: vec.w = lexical_cast<float>(sv, 0); break;
		case 1: vec.x = lexical_cast<float>(sv, 0); break;
		case 2: vec.y = lexical_cast<float>(sv, 0); break;
		case 3: vec.z = lexical_cast<float>(sv, 0); break;
		default: return false;
		}
		return true;
	});
	return vec;
}