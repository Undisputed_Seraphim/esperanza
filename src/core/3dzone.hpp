#pragma once
#include <string>
#include <vector>

#include "simple_vec.hpp"

struct Zone final {
	struct Sound final {
		std::string name;
		std::string file;
		fvec3 position;
		uint8_t volume;
		int radius;
		uint8_t interval;
	};
	std::vector<Sound> m_sounds;

	struct Effector final {
		std::string name;
		std::string name2;
		std::string max_name;
		std::string file;
		std::string model;
		fvec3 position;
		int interval;
		int speed;
		int longitude;
		int latitude;
		fvec3 scale;
		fvec4 rot;
	};
	std::vector<Effector> m_effectors;
};