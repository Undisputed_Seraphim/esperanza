#pragma once

/*
* This header-source pair contains the code needed to parse each data file within the ge/ directory,
* as well as global variables needed to hold those data in memory.
*/
#include <filesystem>
#include <rapidxml.hpp>
#include <string>
#include <unordered_map>
#include <vector>
#include "../ies/ies.hpp"

/* Defined in bg.cpp */
extern std::unordered_map<std::string, std::unique_ptr<rapidxml::xml_document<>>> PROPS;
extern std::unordered_map<std::string, std::unique_ptr<rapidxml::xml_document<>>> WORLDS;
extern std::unordered_map<std::string, std::unique_ptr<rapidxml::xml_document<>>> ZONES;

extern std::unordered_map<std::string, std::string> BGM;

/* Defined in dictionary.cpp */
extern std::vector<std::string> DICTIONARY;

extern std::unordered_map<std::string, IES_Table> IES_TABLES;

void load_directory(const std::filesystem::path&);
