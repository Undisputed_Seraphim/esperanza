#include "data.hpp"
#include "ipf.hpp"
#include <i8n.hpp>
#include <fmemopen.hpp>
#include <string_utils.hpp>
#include <stdexcept>
#include <string_view>
#include <threadpool.hpp>
#include <xml.hpp>

namespace fs = std::filesystem;
using namespace std::literals;

extern threadpool tp;

std::unordered_map<std::string, std::string> BGM;
std::vector<std::string> DICTIONARY;
std::unordered_map<std::string, IES_Table> IES_TABLES;

auto do_ipf_generic(const fs::path& path) -> std::vector<std::string> {
	auto entries = get_all_entries(path);
	auto ret = std::vector<std::string>{};
	for (const auto& [filename, entry] : entries) {
		ret.push_back(filename);
	}
	return ret;
}

auto do_ies_ipf(const fs::path& path) -> std::unordered_map<std::string, IES_Table> {
	auto ret = std::unordered_map<std::string, IES_Table>{};
	if (path.extension() == ".ies"sv) {
		auto table = IES_Table{};
		auto is = std::ifstream(path, std::ios::in | std::ios::binary | std::ios::ate);
		is >> table;
		is.close();
		ret.emplace(path.generic_string(), std::move(table));
	}
	if (path.extension() == ".ipf"sv) {
		auto entries = get_all_entries(path);
		for (const auto& [filename, entry] : entries) {
			auto table = IES_Table{};
			auto buffer = extract_file(path, entry);
			auto is = imemfstream((char*)buffer.data(), buffer.size());
			is >> table;
			is.close();
			ret.emplace(filename, std::move(table));
		}
	}
	return ret;
}

auto do_dictionary_ipf(const fs::path& path) -> std::vector<std::string> {
	if (!fs::exists(path))
		return {};
	using namespace rapidxml;
	auto entries = get_all_entries(path);
	// We only care about dictionary_local.xml.
	auto entry = Entry();
	if (auto& e = entries.find("dictionary_local.xml"); e == entries.end())
		return {};
	else
		entry = e->second;
	auto data = extract_file(path, entry);
	auto f = file<>(imemfstream((char*)data.data(), data.size()));
	auto doc = xml_document<>();
	doc.parse<parse_declaration_node>(f.data());

	decltype(auto) encoding_handler = (std::string_view(doc.first_node()->first_attribute("encoding")->value()) == "SHIFT-JIS")
		? &shiftjis_to_utf8
		: &noop;

	auto dict_node = doc.first_node("Dictionary");
	if (dict_node == 0) {
		throw std::runtime_error("Dictionary entries not found in dictionary_local.xml");
	}

	int count = 0;
	if (auto sv = std::string_view(dict_node->last_node("Text")->first_attribute("ClassID")->value()); !sv.empty()) {
		if (count = lexical_cast<int>(sv, 0); count < 0)
			return {};
	}

	auto dict = std::vector<std::string>((size_t)count + 1);
	node_foreach(dict_node, [&](const rx::xml_node<>& node) {
		int position = lexical_cast<int>(std::string_view(node.first_attribute("ClassID")->value()), -1);
		if (position > -1)
			if (auto attr = node.first_attribute("Text"); attr != NULL)
				dict[position] = std::move(encoding_handler(attr->value()));
		return true;
	});
	return dict;
}

void load_directory(const fs::path& path) {
	for (const auto& filepath : fs::directory_iterator(path)) {
		auto filename = filepath.path().filename().generic_string();
		if (filename.find("animation") == 0) {

		}
		else if (filename.find("bg_common") == 0) {

		}
		else if (filename.find("bg_hi") == 0) {

		}
		else if (filename.find("bg_texture") == 0) {

		}
		else if (filename.find("bg0") == 0 || filename == "bg.ipf") {

		}
		else if (filename.find("bgm") == 0) {

		}
		else if (filename.find("char_hi") == 0) {

		}
		else if (filename.find("char_low") == 0) {

		}
		else if (filename.find("char_mid") == 0) {

		}
		else if (filename.find("char_texture_monster") == 0) {

		}
		else if (filename.find("char_texture_npc") == 0) {

		}
		else if (filename.find("char_texture_pc") == 0) {

		}
		else if (filename.find("char_texture0") == 0 || filename == "char_texture.ipf") {

		}
		else if (filename.find("dictionary") == 0) {
			DICTIONARY = do_dictionary_ipf(filepath);
		}
		else if (filename.find("director") == 0) {

		}
		else if (filename.find("effect_3d") == 0) {

		}
		else if (filename.find("effect_hi") == 0) {

		}
		else if (filename.find("effect_low") == 0) {

		}
		else if (filename.find("etc") == 0) {

		}
		else if (filename.find("font") == 0) {

		}
		else if (filename.find("ies") == 0) {
			IES_TABLES.merge(do_ies_ipf(filepath));
		}
		else if (filename.find("item_hi") == 0) {

		}
		else if (filename.find("item_texture") == 0) {

		}
		else if (filename.find("se") == 0) {

		}
		else if (filename.find("shader") == 0) {

		}
		else if (filename.find("shared") == 0) {

		}
		else if (filename.find("ui") == 0) {

		}
		else if (filename.find("xml") == 0) {

		}
	}
}