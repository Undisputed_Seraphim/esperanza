#pragma once
#include <string>
#include <vector>

#include "simple_vec.hpp"

struct Prop final {
	struct Model final {
		std::string name;
		std::string xref_name;
		std::string type;
		std::string cull_type;
		fvec2 pos;
		float view_dist;
		bool nocol;
		int sort_type;
	};
	std::vector<Model> m_models;

	struct Effect final {
		std::string name;
		std::string xref_name;
		std::string cull_type;
		int view_dist;
		fvec2 interval;
	};
	std::vector<Effect> m_effects;
};