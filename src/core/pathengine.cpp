#include <cstdlib>
#include <charconv>
#include <rapidxml.hpp>
#include <string>
#include <tuple>
#include <vector>
#include "fmemopen.hpp"

#include <iostream>

void read_tok(std::istream& is) {
	enum Type : uint8_t {
		C_STR = 1,
		INT32,
		INT16 = 3,
		INT8,
		UINT32 = 5,
		UINT16,
		UINT8 = 7
	};

	auto strings = std::vector<std::string>();
	auto str = std::string();
	do {
		std::getline(is, str, '\0');
		if (str.empty()) break;
		strings.emplace_back(str);
	}
	while (true);

	auto attrs = std::vector<std::pair<uint8_t, std::string>>();
	char type = 0;
	do {
		is.read(&type, 1);
		if (type == 0)
			break;
		std::getline(is, str, '\0');
		attrs.emplace_back(std::pair{ type, str });
	}
	while (true);
	
	std::cout << "\tStrings\n";
	for (auto& s : strings)
		std::cout << s << '\n';
	std::cout << "\tAttributes\n";
	for (auto& [t, s] : attrs)
		std::cout << s << ' ' << (int)t << '\n';
	std::cout << std::endl;

	char name_id = 0, attr_id = 0;
	do {
		is.read(&name_id, 1);
		if (name_id == '\0')
			continue;
		name_id -= 1;
		std::cout << (int)name_id << ' ' << strings[name_id] << std::endl;

		while (true) {
			is.read(&attr_id, 1);
			if (attr_id == '\0')
				break;
			attr_id -= 1;
			std::cout << "\t" << attrs[attr_id].second << '\t';
			switch (attrs[attr_id].first) {
			case Type::C_STR: {
				std::getline(is, str, '\0');
				std::cout << str << std::endl;
				break;
			}
			case Type::INT32: {
				int32_t val;
				is.read((char*)& val, sizeof(val));
				std::cout << val << std::endl;
				break;
			}
			case Type::INT16: {
				int16_t val;
				is.read((char*)& val, sizeof(val));
				std::cout << (int)val << std::endl;
				break;
			}
			case Type::INT8: {
				int8_t val;
				is.read((char*)& val, sizeof(val));
				std::cout << (int)val << std::endl;
				break;
			}
			case Type::UINT32: {
				uint32_t val;
				is.read((char*)& val, sizeof(val));
				std::cout << val << std::endl;
				break;
			}
			case Type::UINT16: {
				uint16_t val;
				is.read((char*)& val, sizeof(val));
				std::cout << (int)val << std::endl;
				break;
			}
			case Type::UINT8: {
				uint8_t val;
				is.read((char*)& val, sizeof(val));
				std::cout << (int)val << std::endl;
				break;
			}
			default:
				continue;
			}
		}
	}
	while (true);
}

void read_tok(const std::vector<unsigned char>& buffer) {
	return read_tok(imemfstream((char*)buffer.data(), buffer.size()));
}