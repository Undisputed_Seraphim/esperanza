#pragma once
#include <iosfwd>
#include <vector>

#include "3dprop.hpp"
#include "3dworld.hpp"
#include "3dzone.hpp"

Prop read_3dprop(std::istream&);
Prop read_3dprop(const std::vector<unsigned char>&);

Zone read_3dzone(std::istream&);
Zone read_3dzone(const std::vector<unsigned char>&);

World read_3dworld(std::istream&);
World read_3dworld(const std::vector<unsigned char>&);

void do_bg_ipf(const std::filesystem::path& path);