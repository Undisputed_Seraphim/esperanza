#include <filesystem>
#include <iostream>
#include <memory>
#include <string_view>
#include <sstream>
#include <unordered_map>
#include <vector>

#include "3d.hpp"
#include "fmemopen.hpp"
#include "ipf.hpp"
#include "string_utils.hpp"
#include "xml.hpp"

namespace fs = std::filesystem;
using namespace std::literals;

auto read_3dprop(rx::xml_node<>* node) -> Prop {
	auto read_prop_model = [](rx::xml_node<>& node) -> Prop::Model {
		auto ret = Prop::Model{};
		attribute_foreach(node, [&ret](rx::xml_attribute<>& a) -> bool {
			auto name = std::string_view(a.name(), a.name_size());
			auto value = std::string_view(a.value(), a.value_size());
			if (name == "Name") {
				ret.name = a.value();
			}
			else if (iequals(name, "xref_name")) {
				ret.xref_name = a.value();
			}
			else if (name == "Pos") {
				ret.pos = str_to_vec2(a.value());
			}
			else if (name == "ViewDist") {
				ret.view_dist = lexical_cast<float>(a.value(), a.value_size());
			}
			else if (name == "NoCol") {
				ret.nocol = str_to_bool(a.value(), a.value_size());
			}
			else if (name == "SortType") {
				ret.sort_type = lexical_cast<int>(a.value(), a.value_size());
			}
			else if (name == "Type") {
				ret.type = a.value();
			}
			else if (name == "CullType") {
				ret.cull_type = a.value();
			}
			else {
				std::cout << "Unrecognized prop model attribute " << name << std::endl;
			}
			return true;
		});
		return ret;
	};
	auto read_prop_effect = [](rx::xml_node<>& node) -> Prop::Effect {
		auto ret = Prop::Effect{};
		attribute_foreach(node, [&ret](rx::xml_attribute<>& a) -> bool {
			auto name = std::string_view(a.name(), a.name_size());
			if (name == "Name") {
				ret.name = a.value();
			}
			else if (iequals(name, "xref_name")) {
				ret.xref_name = a.value();
			}
			else if (name == "CullType") {
				ret.cull_type = a.value();
			}
			else if (name == "ViewDist") {
				ret.view_dist = lexical_cast<int>(a.value(), a.value_size());
			}
			else if (name == "Interval") {
				ret.interval = str_to_vec2(a.value());
			}
			else {
				std::cout << "Unrecognized prop effect attribute " << name << std::endl;
			}
			return true;
		});
		return ret;
	};

	auto ret = Prop{};
	node_foreach(node, [&](rx::xml_node<>& n) -> bool {
		auto name = std::string_view(n.name(), n.name_size());
		if (name == "Model"sv) {
			ret.m_models.emplace_back(read_prop_model(n));
		}
		else if (name == "Effect"sv) {
			ret.m_effects.emplace_back(read_prop_effect(n));
		}
		else {
			std::cout << "Unrecognized prop node " << name << std::endl;
		}
		return true;
	});
	return ret;
}

auto read_3dzone(rx::xml_node<>* node) -> Zone {
	auto read_zone_sound = [](rx::xml_node<>& node) -> Zone::Sound {
		auto ret = Zone::Sound{};
		attribute_foreach(node, [&ret](rx::xml_attribute<>& a) -> bool {
			auto name = std::string_view(a.name(), a.name_size());
			if (name == "Name") {
				ret.name = a.value();
			}
			else if (name == "File") {
				ret.file = a.value();
			}
			else if (name == "pos") {
				ret.position = str_to_vec3(a.value());
			}
			else if (name == "volume") {
				ret.volume = lexical_cast<uint8_t>(a.value(), a.value_size());
			}
			else if (name == "radius") {
				ret.radius = lexical_cast<int>(a.value(), a.value_size());
			}
			else if (name == "interval") {
				ret.interval = lexical_cast<uint8_t>(a.value(), a.value_size());
			}
			else {
				std::cout << "Unrecognized zone sound attribute " << name << std::endl;
			}
			return true;
		});
		return ret;
	};
	auto read_zone_effector = [](rx::xml_node<>& node) -> Zone::Effector {
		auto ret = Zone::Effector{};
		attribute_foreach(node, [&ret](rx::xml_attribute<>& a) -> bool {
			auto name = std::string_view(a.name(), a.name_size());
			if (name == "Name") {
				ret.name = a.value();
			}
			else if (name == "Name2") {
				ret.name2 = a.value();
			}
			else if (iequals(name, "max_name")) {
				ret.file = a.value();
			}
			else if (name == "File") {
				ret.file = a.value();
			}
			else if (name == "Model") {
				ret.model = a.value();
			}
			else if (name == "pos") {
				ret.position = str_to_vec3(a.value());
			}
			else if (name == "interval") {
				ret.interval = lexical_cast<int>(a.value(), a.value_size());
			}
			else if (name == "speed") {
				ret.speed = lexical_cast<int>(a.value(), a.value_size());
			}
			else if (name == "longitude") {
				ret.longitude = lexical_cast<int>(a.value(), a.value_size());
			}
			else if (name == "latitude") {
				ret.latitude = lexical_cast<int>(a.value(), a.value_size());
			}
			else if (name == "scale") {
				ret.scale = str_to_vec3(a.value());
			}
			else if (name == "rot") {
				ret.rot = str_to_vec4(a.value());
			}
			else {
				std::cout << "Unrecognized zone effector attribute " << name << std::endl;
			}
			return true;
		});
		return ret;
	};

	auto ret = Zone{};
	node_foreach(node, [&](rx::xml_node<>& n) -> bool {
		auto name = std::string_view(n.name(), n.name_size());
		if (name == "Sound"sv) {
			ret.m_sounds.emplace_back(read_zone_sound(n));
		}
		else if (name == "Effector"sv) {
			ret.m_effectors.emplace_back(read_zone_effector(n));
		}
		else {
			std::cout << "Unrecognized zone node " << name << std::endl;
		}
		return true;
	});
	return ret;
}

auto read_3dworld(rx::xml_node<>* node) -> World {
	auto read_loadoption = [](rx::xml_node<>& node) -> World::LoadOption {
		auto ret = World::LoadOption{};
		attribute_foreach(node, [&ret](rx::xml_attribute<>& a) -> bool {
			auto name = std::string_view(a.name(), a.name_size());
			if (name == "Name") {
				ret.use = str_to_bool(a.value(), a.value_size());
			}
			else if (name == "UseCull") {
				ret.use_cull = str_to_bool(a.value(), a.value_size());
			}
			else if (name == "MaxDist") {
				ret.max_dist = lexical_cast<int>(a.value(), a.value_size());
			}
			else if (name == "LoadShaTex") {
				ret.load_shatex = str_to_bool(a.value(), a.value_size());
			}
			else {
				std::cout << "Unrecognized world loadoption attribute " << name << std::endl;
			}
			return true;
		});
		return ret;
	};
	auto read_camera = [](rx::xml_node<>& node) -> World::Camera {
		auto ret = World::Camera{};
		attribute_foreach(node, [&ret](rx::xml_attribute<>& a) -> bool {
			auto name = std::string_view(a.name(), a.name_size());
			if (name == "Fov") {
				ret.fov = lexical_cast<int>(a.value(), a.value_size());
			}
			else if (name == "Near") {
				ret.near = lexical_cast<int>(a.value(), a.value_size());
			}
			else if (name == "Far") {
				ret.far = lexical_cast<int>(a.value(), a.value_size());
			}
			else {
				std::cout << "Unrecognized world camera attribute " << name << std::endl;
			}
			return true;
		});
		return ret;
	};
	auto read_light = [](rx::xml_node<>& node) -> World::Light {
		auto ret = World::Light{};
		attribute_foreach(node, [&ret](rx::xml_attribute<>& a) -> bool {
			auto name = std::string_view(a.name(), a.name_size());
			if (name == "Fov") {
				ret.direction = str_to_vec3(a.value());
			}
			else if (name == "Near") {
				ret.ambient = str_to_vec3<unsigned char>(a.value());
			}
			else if (name == "Far") {
				ret.diffuse = str_to_vec3<unsigned char>(a.value());
			}
			else {
				std::cout << "Unrecognized world light attribute " << name << std::endl;
			}
			return true;
		});
		return ret;
	};
	auto read_lightmap = [](rx::xml_node<>& node) -> World::LightMap {
		auto ret = World::LightMap{};
		attribute_foreach(node, [&ret](rx::xml_attribute<>& a) -> bool {
			auto name = std::string_view(a.name(), a.name_size());
			if (name == "File") {
				ret.file = a.value();
			}
			else if (name == "Length") {
				ret.length = str_to_vec2<int>(a.value());
			}
			else if (name == "Offset") {
				ret.offset = str_to_vec2(a.value());
			}
			else if (name == "Size") {
				ret.size = str_to_vec2<int>(a.value());
			}
			else {
				std::cout << "Unrecognized world lightmap attribute " << name << std::endl;
			}
			return true;
		});
		return ret;
	};
	auto read_fog = [](rx::xml_node<>& node) -> World::Fog {
		auto ret = World::Fog{};
		attribute_foreach(node, [&ret](rx::xml_attribute<>& a) -> bool {
			auto name = std::string_view(a.name(), a.name_size());
			if (name == "Enable") {
				ret.enable = str_to_bool(a.value(), a.value_size());
			}
			else if (name == "Color") {
				ret.color = str_to_vec3<unsigned char>(a.value());
			}
			else if (name == "Near") {
				ret.near = lexical_cast<int>(a.value(), a.value_size());
			}
			else if (name == "Far") {
				ret.far = lexical_cast<int>(a.value(), a.value_size());
			}
			else {
				std::cout << "Unrecognized world fog attribute " << name << std::endl;
			}
			return true;
		});
		return ret;
	};
	auto read_dir = [](rx::xml_node<>& node) -> World::Dir {
		auto ret = World::Dir{};
		attribute_foreach(node, [&ret](rx::xml_attribute<>& a) -> bool {
			auto name = std::string_view(a.name(), a.name_size());
			if (name == "IpfName") {
				ret.ipf_name = a.value();
			}
			else if (name == "Path") {
				ret.path = a.value();
			}
			else {
				std::cout << "Unrecognized world dir attribute " << name << std::endl;
			}
			return true;
		});
		return ret;
	};
	auto read_fixed = [](rx::xml_node<>& node) -> World::Fixed {
		auto ret = World::Fixed{};
		attribute_foreach(node, [&ret](rx::xml_attribute<>& a) -> bool {
			auto name = std::string_view(a.name(), a.name_size());
			if (name == "File") {
				ret.file = a.value();
			}
			else if (name == "Scale") {
				ret.scale = str_to_vec3<int>(a.value());
			}
			else {
				std::cout << "Unrecognized world fixed attribute " << name << std::endl;
			}
			return true;
		});
		return ret;
	};
	auto read_water = [](rx::xml_node<>& node) -> World::Water {
		auto ret = World::Water{};
		attribute_foreach(node, [&ret](rx::xml_attribute<>& a) -> bool {
			auto name = std::string_view(a.name(), a.name_size());
			if (name == "Type") {
				ret.type = a.value();
			}
			else if (name == "Plane") {
				ret.plane = str_to_vec4(a.value());
			}
			else if (name == "Aspect") {
				ret.aspect = lexical_cast<float>(a.value(), a.value_size());
			}
			else if (name == "Scale") {
				ret.scale = str_to_vec2(a.value());
			}
			else if (name == "Offset") {
				ret.offset = str_to_vec2(a.value());
			}
			else {
				std::cout << "Unrecognized world water attribute " << name << std::endl;
			}
			return true;
		});
		return ret;
	};
	auto read_wateroption = [](rx::xml_node<>& node) -> World::WaterOption {
		auto ret = World::WaterOption{};
		attribute_foreach(node, [&ret](rx::xml_attribute<>& a) -> bool {
			auto name = std::string_view(a.name(), a.name_size());
			if (name == "UseReflection") {
				ret.reflection = str_to_bool(a.value(), a.value_size());
			}
			else if (name == "UseRefraction") {
				ret.refraction = str_to_bool(a.value(), a.value_size());
			}
			else if (name == "UseAutoBump") {
				ret.auto_bump = str_to_bool(a.value(), a.value_size());
			}
			else if (name == "WaterColor") {
				ret.color = str_to_vec4(a.value());
			}
			else if (name == "WaterLightDir") {
				ret.light_direction = str_to_vec3(a.value());
			}
			else if (name == "WaterLightDiffuse") {
				ret.light_diffuse = str_to_vec3<unsigned char>(a.value());
			}
			else if (name == "ShadowPower") {
				ret.shadow_power = lexical_cast<int>(a.value(), a.value_size());
			}
			else if (name == "SpecularPower") {
				ret.specular_power = lexical_cast<int>(a.value(), a.value_size());
			}
			else if (name == "ShadowDecrement") {
				ret.shadow_decrement = lexical_cast<float>(a.value(), a.value_size());
			}
			else if (name == "SpecularDecrement") {
				ret.specular_decrement = lexical_cast<float>(a.value(), a.value_size());
			}
			else if (name == "UseFog") {
				ret.fog = str_to_bool(a.value(), a.value_size());
			}
			else if (name == "FogDist") {
				ret.fog_dist = str_to_vec2(a.value());
			}
			else if (name == "FogColor") {
				ret.fog_color = str_to_vec3<unsigned char>(a.value());
			}
			else if (name == "UsePerPixelReflection") {
				ret.per_pixel_reflection = str_to_bool(a.value(), a.value_size());
			}
			else if (name == "ReflectionDiffusion") {
				ret.reflection_diffuse = lexical_cast<float>(a.value(), a.value_size());
			}
			else if (name == "ReflectionOffset") {
				ret.reflection_offset = lexical_cast<float>(a.value(), a.value_size());
			}
			else if (name == "ReflectionDepth") {
				ret.reflection_depth = lexical_cast<float>(a.value(), a.value_size());
			}
			else if (name == "EnableRndChar") {
				ret.enable_rnd_char = str_to_bool(a.value(), a.value_size());
			}
			else if (name == "NmlMapWaveDir") {
				ret.normal_map.wave_direction = str_to_vec2(a.value());
			}
			else if (name == "NmlMapWaveLength") {
				ret.normal_map.wave_length = str_to_vec2<int>(a.value());
			}
			else if (name == "NmlMapWaveChop") {
				ret.normal_map.wave_chop = lexical_cast<int>(a.value(), a.value_size());
			}
			else if (name == "NmlMapWaveRippleScale") {
				ret.normal_map.ripple_scale = lexical_cast<int>(a.value(), a.value_size());
			}
			else if (name == "NmlMapWindAngleDeviation") {
				ret.normal_map.wind_angle_deviation = lexical_cast<int>(a.value(), a.value_size());
			}
			else if (name == "NmlMapWindScale") {
				ret.normal_map.wind_scale = lexical_cast<float>(a.value(), a.value_size());
			}
			else if (name == "NmlMapNoise") {
				ret.normal_map.noise = lexical_cast<float>(a.value(), a.value_size());
			}
			else if (name == "NmlMapUseUVXForm") {
				ret.normal_map.use_uvx_form = str_to_bool(a.value(), a.value_size());
			}
			else if (name == "NmlMapUseRandomWind") {
				ret.normal_map.use_random_wind = str_to_bool(a.value(), a.value_size());
			}
			else if (name == "NmlMapUVOffset") {
				ret.normal_map.uv_offset = str_to_vec4(a.value());
			}
			else {
				std::cout << "Unrecognized world wateroption attribute " << name << std::endl;
			}
			return true;
		});
		return ret;
	};
	auto read_model = [](rx::xml_node<>& node) -> World::Model {
		auto ret = World::Model{};
		attribute_foreach(node, [&ret](rx::xml_attribute<>& a) -> bool {
			auto name = std::string_view(a.name(), a.name_size());
			if (name == "File") {
				ret.file = a.value();
			}
			else if (name == "Model") {
				ret.model = a.value();
			}
			else if (name == "AniFile") {
				ret.anifile = a.value();
			}
			else if (name == "ShadowMap") {
				ret.shadow_map = a.value();
			}
			else if (iequals(name, "xref_name")) {
				ret.xref_name = a.value();
			}
			else if (iequals(name, "max_name")) {
				ret.max_name = a.value();
			}
			else if (name == "pos" || name == "Pos") {
				ret.pos = str_to_vec3(a.value());
			}
			else if (name == "rot") {
				ret.rot = str_to_vec4(a.value());
			}
			else if (name == "scale") {
				ret.scale = str_to_vec3(a.value());
			}
			else {
				std::cout << "Unrecognized world model attribute " << name << std::endl;
			}
			return true;
		});
		return ret;
	};
	auto read_tree = [](rx::xml_node<>& node) -> World::Tree {
		auto ret = World::Tree{};
		attribute_foreach(node, [&ret](rx::xml_attribute<>& a) -> bool {
			auto name = std::string_view(a.name(), a.name_size());
			if (name == "File") {
				ret.file = a.value();
			}
			else if (name == "Model") {
				ret.model = a.value();
			}
			else if (name == "Type") {
				ret.type = a.value();
			}
			else if (name == "pos" || name == "Pos") {
				ret.pos = str_to_vec3(a.value());
			}
			else {
				std::cout << "Unrecognized world tree attribute " << name << std::endl;
			}
			return true;
		});
		return ret;
	};

	auto ret = World{};
	node_foreach(node, [&](rx::xml_node<>& n) -> bool {
		auto name = std::string_view(n.name(), n.name_size());
		if (name == "LoadingOption"sv) {
			ret.m_loadoption = read_loadoption(n);
		}
		else if (name == "Camera"sv) {
			ret.m_camera = read_camera(n);
		}
		else if (name == "Light"sv) {
			ret.m_light = read_light(n);
		}
		else if (name == "LightMap"sv) {
			ret.m_lightmap = read_lightmap(n);
		}
		else if (name == "Fog"sv) {
			ret.m_fog = read_fog(n);
		}
		else if (name == "ModelDir"sv) {
			ret.m_model = read_dir(n);
		}
		else if (name == "TexDir"sv) {
			ret.m_texture = read_dir(n);
		}
		else if (name == "ShaTexDir"sv) {
			ret.m_shadowtexture = read_dir(n);
		}
		else if (name == "SubModelDir"sv) {
			ret.m_submodel = read_dir(n);
		}
		else if (name == "SubTexDir"sv) {
			ret.m_subtexture = read_dir(n);
		}
		else if (name == "SubShaTexDir"sv) {
			ret.m_subshadowtexture = read_dir(n);
		}
		else if (name == "Fixed"sv) {
			ret.m_fixed = read_fixed(n);
		}
		else if (name == "Water"sv) {
			ret.m_water = read_water(n);
		}
		else if (name == "WaterOption"sv) {
			ret.m_water_option = read_wateroption(n);
		}
		else if (name == "Model"sv) {
			ret.m_models.emplace_back(read_model(n));
		}
		else if (name == "Tree"sv) {
			ret.m_trees.emplace_back(read_tree(n));
		}
		else {
			std::cout << "Unrecognized world node " << name << std::endl;
		}
		return true;
	});
	return ret;
}

auto read_3dprop(std::istream& is) -> Prop {
	auto f = rx::file<>(is);
	auto doc = rx::xml_document<>();
	doc.parse<rx::parse_fastest>(f.data());
	auto node = doc.first_node("Prop");
	return (node != 0) ? read_3dprop(node) : Prop{};
}

auto read_3dprop(const std::vector<unsigned char>& buffer) -> Prop {
	return read_3dprop(imemfstream((char*)buffer.data(), buffer.size()));
}

auto read_3dzone(std::istream& is) -> Zone {
	auto f = rx::file<>(is);
	auto doc = rx::xml_document<>();
	doc.parse<rx::parse_fastest>(f.data());
	auto node = doc.first_node("Zone");
	return (node != 0) ? read_3dzone(node) : Zone{};
}

auto read_3dzone(const std::vector<unsigned char>& buffer) -> Zone {
	return read_3dzone(imemfstream((char*)buffer.data(), buffer.size()));
}

auto read_3dworld(std::istream& is) -> World {
	auto f = rx::file<>(is);
	auto doc = rx::xml_document<>();
	doc.parse<rx::parse_fastest>(f.data());
	auto node = doc.first_node("World");
	return (node != 0) ? read_3dworld(node) : World{};
}

auto read_3dworld(const std::vector<unsigned char>& buffer) -> World {
	return read_3dworld(imemfstream((char*)buffer.data(), buffer.size()));
}

std::unordered_map<std::string, std::unique_ptr<rx::xml_document<>>> PROPS = {};
std::unordered_map<std::string, std::unique_ptr<rx::xml_document<>>> WORLDS = {};
std::unordered_map<std::string, std::unique_ptr<rx::xml_document<>>> ZONES = {};
static std::vector<rx::file<>> _files;

// Entry point of this unit
void do_bg_ipf(const fs::path& path) try {
	_files = {};
	const auto entries = get_all_entries(path);
	auto ifs = std::ifstream(path, std::ios::in | std::ios::binary);
	for (auto& [filename, entry] : entries) {
		auto content = extract_file(ifs, entry);
		auto is = imemfstream((char*)content.data(), content.size());
		auto f = rx::file<>(is);
		_files.push_back(std::move(f));
		auto doc = std::make_unique<rx::xml_document<>>();
		doc->parse<rx::parse_fastest>(f.data());
		if (auto* node = doc->first_node("Prop"); node != 0) {
			//PROPS.emplace(filename, doc);
			PROPS[filename] = std::move(doc);
			continue;
		}
		if (auto* node = doc->first_node("World"); node != 0) {
			//WORLDS.emplace(filename, doc);
			WORLDS[filename] = std::move(doc);
			continue;
		}
		if (auto* node = doc->first_node("Zone"); node != 0) {
			//ZONES.emplace(filename, doc);
			ZONES[filename] = std::move(doc);
			continue;
		}
		is.close();
	}
}
catch (const std::exception& e) {
	std::cerr << "Exception caught" << std::endl;
	std::cerr << e.what() << std::endl;
}