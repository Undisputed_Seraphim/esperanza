#include <args.hxx>
#include <filesystem>
#include <threadpool.hpp>
#include <regex>
#include <unordered_map>
#include <vector>
#include "data.hpp"
#include "ge.hpp"
#include "ipf.hpp"
#include "3d.hpp"
#include "../utils/fmemopen.hpp"

#include <iostream>

using namespace std::literals;
namespace fs = std::filesystem;

threadpool tp = threadpool();

extern void read_tok(const std::vector<unsigned char>& buffer);

int main(int argc, char* argv[]) {
	auto parser = args::ArgumentParser("Esperanza is a Granado Espada data file parser.", "");
	auto help = args::HelpFlag(parser, "help", "Show this help message and exit.", { 'h', "help" });
	auto group = args::Group(parser, "", args::Group::Validators::Xor);
	auto dump = args::ValueFlag<std::string>(group, "path", "Dump the IPF file list to stdout.", { 'd', "dump" });
	auto extract = args::ValueFlag<std::string>(group, "path", "Extract all files from the specified IPF file.", { 'x', "extract" });
	auto f = args::CompletionFlag(parser, { "complete" });

	try {
		parser.ParseCLI(argc, argv);
	}
	catch (const args::Help&) {
		std::cout << parser;
		return EXIT_SUCCESS;
	}
	catch (const args::Completion& e) {
		std::cout << e.what();
		return EXIT_SUCCESS;
	}
	catch (const std::exception& e) {
		std::cerr << e.what() << std::endl;
		std::cerr << parser;
		return EXIT_FAILURE;
	}

	auto path = fs::path();
	if (dump)
		path = fs::path(args::get(dump));
	else if (extract)
		path = fs::path(args::get(extract));
	else
		return EXIT_FAILURE;
	if (!fs::exists(path)) {
		std::cerr << "Path does not exist." << std::endl;
		return EXIT_FAILURE;
	}
	
	if (fs::is_regular_file(path)) {
		if (extract) {
			auto ifs = std::ifstream(path, std::ios::in | std::ios::binary);
			auto entries = get_all_entries(path);
			path.replace_extension("");
			fs::create_directory(path);
			for (const auto& [name, entry] : entries) {
				auto data = extract_file(ifs, entry);
				auto dest = (path / name).make_preferred();
				if (!fs::exists(dest.parent_path()))
					fs::create_directories(dest.parent_path());
				std::cout << "Extracting " << name << " from " << path << " to " << dest << std::endl;
				std::ofstream(dest, std::ios::out | std::ios::binary)
					.write((char*)data.data(), data.size())
					.flush();
			}
		}
	}
	return EXIT_SUCCESS;
}