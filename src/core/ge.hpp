#pragma once
#include <filesystem>
#include <string>
#include <vector>

#include "../ies/ies.hpp"

namespace fs = std::filesystem;

auto do_dictionary_ipf(fs::path& path)->std::vector<std::string>;