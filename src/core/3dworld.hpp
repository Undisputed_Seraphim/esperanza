#pragma once
#include <string>
#include <vector>

#include "simple_vec.hpp"

constexpr auto default_sca = fvec3{ 1.0f, 1.0f, 1.0f };
constexpr auto default_rot = fvec4{ -0.0f, -0.0f, -0.0f, 1.0f };

struct World final {
	struct Model final {
		std::string file;
		std::string model;
		std::string anifile;
		std::string shadow_map;
		std::string xref_name;
		std::string max_name;
		fvec3 pos, scale = default_sca;
		fvec4 rot = default_rot;

		std::string name;
		std::string cull_type;
		std::string type;
		bool no_col;
		int view_dist;
	};
	std::vector<Model> m_models;

	struct Tree final {
		std::string file;
		std::string model;
		std::string type;
		fvec3 pos;
	};
	std::vector<Tree> m_trees;

	struct LoadOption final {
		bool use;
		bool use_cull;
		bool load_shatex;
		int max_dist;
	} m_loadoption;

	struct Camera final {
		int fov;
		int near, far;
	} m_camera;

	struct Light final {
		fvec3 direction;
		ucvec3 ambient, diffuse;
	} m_light;

	struct LightMap final {
		std::string file;
		fvec2 offset;
		ivec2 length, size;
	} m_lightmap;

	struct Fog final {
		bool enable;
		ucvec3 color;
		int near, far;
	} m_fog;

	struct Dir final {
		std::string ipf_name;
		std::string path;
	} m_model, m_submodel, m_texture, m_subtexture, m_subshadow, m_shadowtexture, m_subshadowtexture;

	struct Fixed final {
		std::string file;
		vec3 scale;
	} m_fixed;

	struct Water final {
		std::string type;
		fvec4 plane;
		float aspect;
		fvec2 scale;
		fvec2 offset;
	} m_water;

	struct WaterOption final {
		bool reflection, refraction, auto_bump, vertex_anim, fog, per_pixel_reflection;
		fvec4 color;
		fvec3 light_direction;
		ucvec3 light_diffuse;
		fvec2 fog_dist;
		ucvec3 fog_color;
		int shadow_power, specular_power;
		float shadow_decrement, specular_decrement;
		float reflection_diffuse, reflection_offset, reflection_depth;

		struct NormalMap final {
			fvec2 wave_direction;
			ivec2 wave_length;
			int wave_chop, ripple_scale, wind_angle_deviation;
			float wind_scale;
			float noise;
			bool use_uvx_form, use_random_wind;
			fvec4 uv_offset;
		} normal_map;
		bool enable_rnd_char;
	} m_water_option;

	struct Physics final {
		float tree_wind_strength;
	} m_physics;

	struct TreeLodFactor final {
		float near, far;
	} m_treelodfactor;

	struct Effect final {
		std::string name;
		std::string cull_type;
		bool no_col;
		int view_dist;
	};
	std::vector<Effect> m_effects;

	struct Sound final {
		std::string name;
		std::string file;
		fvec3 position;
		uint8_t volume;
		int radius;
		uint8_t interval;
	};
	std::vector<Sound> m_sounds;

	struct Effector final {
		std::string max_name;
		std::string name;
		std::string file;
		fvec3 position;
		int interval;
		int speed;
		int longitude;
		int latitude;
		fvec3 scale;
		int attr_viewdist;
	};
	std::vector<Effector> m_effectors;
};