#pragma once

#include <glm/glm.hpp>
#include <xml.hpp>

// Converts a string of (%f %f %f) into vec3
auto text_to_vec3(std::string_view text) noexcept->glm::fvec3;

// Converts a string of (%f %f %f %f) into vec4
auto text_to_vec4(std::string_view text) noexcept->glm::fvec4;