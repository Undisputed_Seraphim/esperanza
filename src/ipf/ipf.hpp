#pragma once
#include <cstdint>
#include <filesystem>
#include <fstream>
#include <unordered_map>

struct Entry final {
	int32_t offset;
	int32_t compressed_size;
	int32_t decompressed_size;
};

auto get_all_entries(const std::filesystem::path&)->std::unordered_map<std::string, Entry>;

auto extract_file(std::ifstream&, const Entry&)->std::vector<unsigned char>;

auto extract_file(const std::filesystem::path&, const Entry&)->std::vector<unsigned char>;
