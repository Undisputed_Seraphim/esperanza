#include <array>
#include <charconv>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <regex>
#include <string>
#include <string_view>
#include <unordered_map>
#include <utility>

#include <rapidxml.hpp>
#include <rapidxml_iterators.hpp>
#include <rapidxml_utils.hpp>

namespace fs = std::filesystem;
namespace rx = rapidxml;
using namespace std::literals;

template <typename Callback>
void node_foreach(rx::xml_node<>* node, Callback&& cb) {
	for (auto i = rx::node_iterator<char>(node); i != decltype(i)(); ++i) {
		if (!cb(*i)) break;
	}
}

template<typename Callback>
void attribute_foreach(rx::xml_node<>& node, Callback&& cb) {
	for (auto i = rx::attribute_iterator<char>(&node); i != decltype(i)(); ++i) {
		if (!cb(*i)) break;
	}
}

inline void inplace_tolower(std::string& str) {
	std::transform(str.begin(), str.end(), str.begin(), [](char c) {return ::tolower(c); });
}

inline void ltrim(std::string& s) {
	s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
		return !std::isspace(ch);
	}));
}

inline void rtrim(std::string& s) {
	while (std::isspace(s.back())) {
		s.pop_back();
	}
}

inline void trim(std::string& s) {
	if (s.empty()) return;
	ltrim(s);
	rtrim(s);
}

int regex_match_to_int(const std::string& input) {
	static const auto regex = std::regex("^\<\$\>(\d+)\<\/\>");
	auto match = std::smatch();
	int ret = -1;
	if (std::regex_match(input, match, regex) && match.ready()) {
		auto cast = std::from_chars(input.c_str() + match.position(), input.c_str() + match.position() + match.length(), ret);
		if (cast.ec == std::errc()) {

		}
	}
	return ret;
}

auto guess_type(const std::string& str) -> std::string {
	if (str.empty()) return "";
	static const auto matchers = std::unordered_map<std::string, std::regex>{
		{"fvec4", std::regex("^(\\-*\\d+\\.\\d+ +){3}(\\-*\\d+\\.\\d+)$")},
		{"ivec4", std::regex("^(\\-*\\d+ +){4}\\-*\\d+$")},
		{"fvec3", std::regex("^(\\-*\\d+\\.\\d+ +){2}(\\-*\\d+\\.\\d+)$")},
		{"ivec3", std::regex("^(\\-*\\d+ +){2}\\-*\\d+$")},
		{"fvec2", std::regex("^(\\-*\\d+\\.\\d+ +){1}(\\-*\\d+\\.\\d+)$")},
		{"ivec2", std::regex("^(\\-*\\d+ +){1}\\-*\\d+$")},
		{"float", std::regex("^\\-*\\d+\\.\\d+$")},
		{"int32_t", std::regex("^\\-*\\d+$")},
		{"direction", std::regex("^((left|right|center|top|bottom) *)+$")},
		//{"path", std::regex("^(\\w+(\\/|\\\))+(\\w+\\.\\w+)$")}
		//{"caption", std::regex("^\\<\\$\\>(\\d+)\\<\\/\\>")}
	};
	static const auto boolean = std::array{
		"yes"s, "no"s,
		"true"s, "false"s,
		"on"s, "off"s,
	};
	auto cmp_ign_cse = [&str](std::string_view r) -> bool {
		if (str.size() != r.size()) return false;
#ifdef _WIN32
		return ::_strnicmp(str.data(), r.data(), str.size()) == 0;
#else
		return ::strncasecmp(str.data(), r.data(), str.size()) == 0;
#endif
	};
	for (const auto& [type, regex] : matchers) {
		if (std::regex_match(str, regex)) {
			return type;
		}
	}
	if (std::any_of(boolean.begin(), boolean.end(), cmp_ign_cse)) {
		return "bool";
	}
	return "std::string";
}

// from type to type
static auto overridden_types = std::unordered_map<std::string, std::string>{
	{"int32_t", "float"},
	{"bool", " int32_t"},
};

class class_builder final {
public:
	class_builder() : name({}), fields({}) {}
	class_builder(const std::string& name_) : name(name_), fields({}) {}
	//class_builder(const class_builder&) = delete;
	class_builder(class_builder&&) noexcept = default;
	~class_builder() noexcept {
		for (auto& e : subclasses) {
			delete e.second;
		}
	}

	std::string name;
	std::unordered_map<std::string, class_builder*> subclasses;
	std::map<std::string, std::string> fields; // name-type
	//std::multimap<std::string, std::string> mfields = {}; // type-name

	void collect_fields(rx::xml_node<>& n) {
		attribute_foreach(n, [this](rx::xml_attribute<>& a) -> bool {
			auto name = std::string(a.name(), a.name_size());
			auto valu = std::string(a.value(), a.value_size());
			trim(valu);
			auto type = guess_type(valu);
			inplace_tolower(name);
			if (type.empty()) return true;
			if (fields.find(name) != fields.end()) {

				for (const auto& [first_guess, last_guess] : overridden_types) {
					if (fields[name] == last_guess && type == first_guess) {
						fields[name] = type;
					}
				}

				auto& last_type = fields[name];
				if (last_type.empty()) {
					last_type = type;
				}
				else if (last_type == "std::string" && type != "std::string") {
					//std::cerr << name << '\t' << last_type << '\t' << type << '\t' << valu << std::endl;
					// noop
				}
				// if we previously guessed float but we guessed int32_t now, keep the float
				else if (last_type == "float" && type == "int32_t") {
					// noop
				}
				// if we previously guessed int32_t but we guessed float now, override last guess
				else if (last_type == "int32_t" && type == "float") {
					last_type = type;
				}
				else if (fields[name] == "bool" && type == "int32_t") {
					last_type = type;
				}
				else if (last_type != type) {
					std::cerr << name << '\t' << last_type << '\t' << type << '\t' << valu << std::endl;
					fields[name] = "std::string";
				}
			}
			else {
				fields[name] = type;
			}
			return true;
		});
	}
};

int main(int argc, char* argv[]) try {
	if (argc < 2) {
		return 1;
	}
	auto klasses = std::unordered_map<std::string, class_builder>{};
	auto root = fs::path(argv[1]);
	for (const auto& file : fs::recursive_directory_iterator(root)) {
		auto path = file.path();
		if (path.extension() != ".xml") {
			continue;
		}
		auto xmlfile = rx::file<>(path.generic_string().c_str());
		auto doc = rx::xml_document<>();
		try {
			doc.parse<rx::parse_fastest>(xmlfile.data());
		}
		catch (const std::exception& e) {
			std::cerr << "Exception caught when parsing " << path << std::endl;
			std::cerr << e.what() << std::endl;
			continue;
		}
		std::cerr << "In file " << file << std::endl;
		node_foreach(&doc, [&klasses](rx::xml_node<>& n) -> bool {
			auto name = std::string(n.name(), n.name_size());
			inplace_tolower(name);
			if (klasses.find(name) == klasses.end()) {
				klasses.emplace(name, class_builder(name));
			}
			auto& klass = klasses[name];
			klass.collect_fields(n);
			node_foreach(&n, [&klass](rx::xml_node<>& nn) -> bool {
				auto subclass_name = std::string(nn.name(), nn.name_size());
				inplace_tolower(subclass_name);
				if (klass.subclasses.find(subclass_name) == klass.subclasses.end()) {
					klass.subclasses.emplace(subclass_name, new class_builder(subclass_name));
				}
				auto& subklass = klass.subclasses[subclass_name];
				subklass->collect_fields(nn);
				if (nn.value_size() > 0) {
					subklass->fields["_value"] = "std::string";
				}
				klass.fields[subclass_name + 's'] = "std::vector<" + subclass_name + ">";
				return true;
			});
			if (n.value_size() > 0) {
				klass.fields["_value"] = "std::string";
			}
			return true;
		});
	}
	
	for (const auto& [classname, klass] : klasses) {
		std::cout << "struct " << classname << " {\n";
		for (const auto& [subclassname, subclass] : klass.subclasses) {
			std::cout << "\tstruct " << subclassname << " {\n";
			for (const auto& [fieldname, field] : subclass->fields) {
				std::cout << "\t\t" << field << ' ' << fieldname << ";\n";
			}
			std::cout << "\t};\n";
		}
		std::cout << '\n';
		for (const auto& [fieldname, field] : klass.fields) {
			std::cout << '\t' << field << ' ' << fieldname << ";\n";
		}
		std::cout << "};\n" << std::endl;
	}
	return 0;
}
catch (const std::exception& e) {
	std::cerr << e.what() << std::endl;
}
catch (...) {
	std::cerr << "Unknown exception caught." << std::endl;
}