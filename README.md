# Esperanza

A Granado Espada object file viewer.

## Building
- C++17 compiler
- CMake
- zlib

## License
GPLv3
